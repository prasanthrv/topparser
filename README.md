# TopParser

TopParser is parser for parsing the output of the Top command in *nix like systems.

  - Parses according to platform (OSX implemented for now)
  - Is built as shared library, see `src/CMakeLists.txt`
  - Uses CMake for cross platform compatibility

**Header Files:**  

  * TopParser.h -- TopParser is an abstract class for common platform independent parsing   
  * MacParser.h -- OSX specific parsing implementation  
  * TopStructures.h -- Structures to hold `top` information  
  * MemTypes.h -- Memory related structures  
  
Use `MacParser` to parse logs in either samples or as one log. Add needed implementations by subclassing `TopParser` and `TopData`, dynamic cast at runtime to needed conversion.

**Output Directories:**  

  * {build_dir}/lib -- for library module
  * {build_dir}/include -- for include directory
  * Comment `set (CMAKE_INSTALL_PREFIX "${CMAKE_BINARY_DIR}" )` in `{root}/CMakeLists.txt` for installing system wide 
  
**Build Instructions:**   

  * `cmake {source_dir}`
  * `make, make install`
  * Building release version, `cmake -DCMAKE_BUILD_TYPE=Release {source_dir} `