//
// Created by Prasanth Ravi on 05/08/16.
//


#include "MacTopParser.h"

TopData MacTopParser::parse_data(const string& log) {

    cout << log << endl;
    auto data_vector = split_string(log, "\n");

    forward_list<string> data;
    data.assign(data_vector.begin(), data_vector.end());

    MacTopData top_data;

    top_data.process_info = parse_proc_info(data);
    top_data.time = parse_time_date(data);
    top_data.load_average = parse_load_avg(data);
    top_data.cpu_usage = parse_cpu_usage(data);
    top_data.shared_libs = parse_shared_libs(data);
    top_data.mem_regions = parse_mem_regions(data);
    top_data.physical_mem = parse_phy_mem(data);
    top_data.virtual_mem = parse_vir_mem(data);
    top_data.networks = parse_networks(data);
    top_data.disks = parse_disks(data);

    auto headers = parse_proc_headers(data);
    auto proc_map = proc_data_to_map(data, headers);
    top_data.process_list = parse_proc_data(proc_map);

    return top_data;
}
vector<ProcessData> MacTopParser::parse_proc_data(data_map &data_map) {

    vector<ProcessData> result;

    for (auto& item: data_map)
    {
        MacProcessData proc_data;

        proc_data.process_id = atoi(item["PID"].c_str());
        proc_data.command = item["COMMAND"];
        proc_data.cpu_percent = atof(item["%CPU"].c_str());
        proc_data.time = parse_time(item["TIME"].c_str());
        proc_data.thread_count = atoi(item["#TH"].c_str());
        proc_data.workqueue_count = atoi(item["#WQ"].c_str());
        proc_data.ports = atoi(item["#PORTS"].c_str());
        proc_data.mem = parse_mem(item["MEM"].c_str());
        proc_data.mem_purge = parse_mem(item["PURG"].c_str());
        proc_data.mem_compressed = parse_mem(item["CMPRS"].c_str());
        proc_data.proc_group = atoi(item["PGRP"].c_str());
        proc_data.parent_process_id = atoi(item["PPID"].c_str());
        proc_data.state = parse_proc_state(item["STATE"]);
        proc_data.boosts = parse_boosts(item["BOOSTS"]);
        proc_data.cpu_me = atof(item["%CPU_ME"].c_str());
        proc_data.cpu_others = atof(item["%CPU_OTHRS"].c_str());
        proc_data.user_id = atoi(item["UID"].c_str());
        proc_data.faults = atoi(item["FAULTS"].c_str());
        proc_data.cow = atoi(item["COW"].c_str());
        proc_data.msg_recv = atoi(item["MSGSENT"].c_str());
        proc_data.msg_recv = atoi(item["MSFRECV"].c_str());
        proc_data.sys_bsd = atoi(item["SYSBSD"].c_str());
        proc_data.sys_mach = atoi(item["SYSBSD"].c_str());
        proc_data.csw = atoi(item["CSW"].c_str());
        proc_data.page_ins = atoi(item["PAGEINS"].c_str());
        proc_data.idlew = atoi(item["IDLEW"].c_str());
        proc_data.power = atof(item["POWER"].c_str());
        proc_data.user = item["USER"];

        proc_data.memory_regions = atoi(item["#MREGS"].c_str());
        proc_data.resident_private = atoi(item["RPRVT"].c_str());
        proc_data.virtual_private = atoi(item["VPRVT"].c_str());
        proc_data.virtual_size = atoi(item["VSIZE"].c_str());
        proc_data.kernel_private = atoi(item["KPRVT"].c_str());
        proc_data.kernel_shared = atoi(item["KSHRD"].c_str());

        result.push_back(proc_data);
    }

    return result;
}

Boosts MacTopParser::parse_boosts(const string &str)
{
    auto fields = split_string(str, "[");
    Boosts result;

    result.sent_boost = str.find("*") != string::npos;
    result.boost_count = atoi(fields[0].c_str());
    result.transition_count = atoi(fields[1].c_str());

    return result;
}


