//
// Created by Prasanth Ravi on 05/08/16.
//

#ifndef POSIXMONITOR_MACTOPPARSER_H
#define POSIXMONITOR_MACTOPPARSER_H



#include <iomanip>
#include <stdlib.h>
#include <sstream>
#include <iostream>

#include "MemTypes.h"
#include "TopParser.h"
#include "utils.h"

using namespace std;

class MacTopParser : public TopParser {

public:
    // mac specific parsing
    TopData parse_data(const string& log) override;


private:
    typedef forward_list<string> data_list ;
    // boosts for OSX
    Boosts          parse_boosts        (const string& str);

protected:
    // cast ProcessData to MacProcessData
    vector<ProcessData> parse_proc_data(data_map &data_map) override;

};


#endif //POSIXMONITOR_MACTOPPARSER_H
