//
// Created by Prasanth Ravi on 02/08/16.
//

#include <cstdlib>
#include <string>
#include "MemTypes.h"
using namespace std;

double MemValue::to_GB()
{
    switch(type)
        {
            case MemType::B:
                return value * b_to_gb;
            case MemType::KB:
                return value * kb_to_gb;
            case MemType::MB:
                return value * mb_to_gb;
            default:
                return value;
        }
}

double MemValue::to_MB()
{
    switch(type)
    {
        case MemType::B:
            return value * b_to_mb;
        case MemType::KB:
            return value * kb_to_mb;
        case MemType::GB:
            return value * gb_to_mb;
        default:
            return value;
    }
}

double MemValue::to_KB()
{
    switch(type)
    {
        case MemType::B:
            return value * b_to_kb;
        case MemType::MB:
            return value * mb_to_kb;
        case MemType::GB:
            return value * gb_to_kb;
        default:
            return value;
    }
}

double MemValue::to_Bytes()
{
    switch(type)
    {
        case MemType::KB:
            return value * kb_to_b;
        case MemType::MB:
            return value * mb_to_b;
        case MemType::GB:
            return value * gb_to_b;
        default:
            return value;
    }
}

MemValue parse_mem(string mem_text) {
    long mem_val = atol(mem_text.c_str());
    MemValue result;
    result.value = mem_val;

    bool is_gb = mem_text.find("G") != string::npos;
    bool is_mb = mem_text.find("M") != string::npos;
    bool is_kb = mem_text.find("K") != string::npos;

    if (is_gb)
        result.type = MemType::GB;
    else if (is_mb)
        result.type = MemType::MB;
    else if (is_kb)
        result.type = MemType::KB;
    else
        result.type = MemType::B;

    return result;
}

