//
// Created by Prasanth Ravi on 02/08/16.
//

#ifndef POSIXMONITOR_MEMTYPES_H
#define POSIXMONITOR_MEMTYPES_H

#include <math.h>

enum class MemType
{
    B,  //     Byte
    KB, //Kilo-Byte
    MB, //Mega-Byte
    GB  //Giga-Byte
};

class MemValue final
{
public:
    double value;
    MemType type;

    double to_GB();
    double to_MB();
    double to_KB();
    double to_Bytes();

private:

    // conversion ratios
    static constexpr double base = 1024;
    static constexpr double gb_to_b = base * base * base;
    static constexpr double gb_to_kb = base * base;
    static constexpr double gb_to_mb = base;

    static constexpr double mb_to_b = base * base;
    static constexpr double mb_to_kb = base;
    static constexpr double mb_to_gb = 1 / base;

    static constexpr double kb_to_b = base;
    static constexpr double kb_to_mb = 1 / base;
    static constexpr double kb_to_gb  = 1 / (base * base);

    static constexpr double b_to_kb = 1/ base;
    static constexpr double b_to_mb = 1 / (base * base);
    static constexpr double b_to_gb = 1 / (base * base * base);

};

// parse string to mem value
MemValue parse_mem(std::string);

#endif //POSIXMONITOR_MEMTYPES_H
