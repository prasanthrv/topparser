//
// Created by Prasanth Ravi on 01/08/16.
//

#include <iostream>
#include <assert.h>
#include <sstream>
#include <iomanip>
#include "TopParser.h"
#include "utils.h"



string TopParser::string_from_file(FILE *file)
{

    string result;

    int c;

    while((c = fgetc(file)) != EOF)
    {
        result += c;
    }

    return result;

}

ProcessInfo TopParser::parse_proc_info(forward_list<string, allocator<string>> &data) {
    string line = data.front();
    data.pop_front();

    auto fields = split_string(line, " ");

    assert(fields.size() > 9);

    ProcessInfo proc_info;
    proc_info.total = atoi(fields[1].c_str());
    proc_info.running = atoi(fields[3].c_str());
    proc_info.stuck = atoi(fields[5].c_str());
    proc_info.sleeping = atoi(fields[7].c_str());
    proc_info.thread_count = atoi(fields[9].c_str());

    return proc_info;
}

tm TopParser::parse_time_date(forward_list<string, allocator<string>> &data) {
    string line = data.front();
    data.pop_front();

    string format = "%y/%m/%d %h:%m:%s";
    tm time;

    istringstream is(line.c_str());
    is >> get_time(&time, format.c_str());

    return time;
}

LoadAvg TopParser::parse_load_avg(forward_list<string, allocator<string>> &data) {
    string line = data.front();
    data.pop_front();

    auto fields = split_string(line, " ");
    LoadAvg load_avg;
    load_avg.one_min = atof(fields[2].c_str());
    load_avg.five_min = atof(fields[3].c_str());
    load_avg.fifteen_min = atof(fields[4].c_str());

    return load_avg;
}

CpuUsage TopParser::parse_cpu_usage(forward_list<string, allocator<string>> &data) {
    string line = data.front();
    data.pop_front();

    auto fields = split_string(line, " ");
    CpuUsage cpu_usage;
    cpu_usage.user = atof(fields[2].c_str());
    cpu_usage.sys = atof(fields[4].c_str());
    cpu_usage.idle = atof(fields[6].c_str());

    return cpu_usage;
}

SharedLibs TopParser::parse_shared_libs(forward_list<string, allocator<string>> &data) {
    string line = data.front();
    data.pop_front();

    auto fields = split_string(line, " ");
    SharedLibs shared_libs;
    shared_libs.resident = parse_mem(fields[1].c_str());
    shared_libs.data = parse_mem(fields[3].c_str());
    shared_libs.linkedit = parse_mem(fields[5].c_str());

    return shared_libs;
}

MemRegions TopParser::parse_mem_regions(forward_list<string, allocator<string>> &data) {
    string line = data.front();
    data.pop_front();

    auto fields = split_string(line, " ");
    MemRegions mem_regions;
    mem_regions.total = atoi(fields[1].c_str());
    mem_regions.resident = parse_mem(fields[3].c_str());
    mem_regions.pvt = parse_mem(fields[5].c_str());
    mem_regions.shared = parse_mem(fields[7].c_str());

    return mem_regions;
}

PhysicalMem TopParser::parse_phy_mem(forward_list<string, allocator<string>> &data) {
    string line = data.front();
    data.pop_front();

    auto fields = split_string(line, " ");
    PhysicalMem phy_mem;
    phy_mem.used = parse_mem(fields[1].c_str());
    phy_mem.wired = parse_mem(fields[3].c_str());
    phy_mem.unused = parse_mem(fields[5].c_str());

    return phy_mem;
}

VirtualMem TopParser::parse_vir_mem(forward_list<string, allocator<string>> &data) {
    string line = data.front();
    data.pop_front();

    auto fields = split_string(line, " ");
    VirtualMem vir_mem;
    vir_mem.vsize = parse_mem(fields[1].c_str());
    vir_mem.framework_vsize = parse_mem(fields[3].c_str());

    //tbd swaps

    return vir_mem;
}

Networks TopParser::parse_networks(forward_list<string, allocator<string>> &data) {
    string line = data.front();
    data.pop_front();

    auto fields = split_string(line, " ");
    auto pkt_in = split_string(fields[2], "/");
    auto pkt_out = split_string(fields[4], "/");

    Networks networks;
    networks.in_packets_count = atol(pkt_in[0].c_str());
    networks.in_total_size = parse_mem(pkt_in[1].c_str());
    networks.out_packets_count = atol(pkt_out[0].c_str());
    networks.out_total_size = parse_mem(pkt_out[1].c_str());
    return networks;
}

Disks TopParser::parse_disks(forward_list<string, allocator<string>> &data) {
    string line = data.front();
    data.pop_front();

    auto fields = split_string(line, " ");
    auto read = split_string(fields[1], "/");
    auto write = split_string(fields[3], "/");

    Disks disks;
    disks.read_count = atol(read[0].c_str());
    disks.read_size = parse_mem(read[1].c_str());
    disks.write_count = atol(write[0].c_str());
    disks.write_size = parse_mem(write[1].c_str());
    return disks;
}

ProcessState TopParser::parse_proc_state(string str) {
    if (str == "running")
        return ProcessState::running;
    else if (str == "sleeping")
        return ProcessState::sleeping;
    else if (str == "stuck")
        return ProcessState::stuck;
    else
        return ProcessState::dead;
}


forward_list<string>
TopParser::parse_proc_headers(forward_list<string, allocator<string>> &data) {
    string line = data.front();
    data.pop_front();

    auto fields = split_string(line, " \t");
    forward_list<string> headers(fields.begin(), fields.end());
    return headers;
}

forward_list<unordered_map<string,string>>
TopParser::proc_data_to_map(forward_list<string, allocator<string>> &data,
                            const data_list &headers) {
    data_map result;
    while(!data.empty())
    {
        string line = data.front();
        line = trim_string(line);

        if (line == "") break;
        data.pop_front();

        unordered_map<string, string> map;
        auto fields = split_string(line," \t");

        assert(list_size(headers) == (fields.size() - 1));
        for(auto index = make_pair(headers.begin(),0);
            index.first != headers.end(); index.first++, index.second++)
           map.insert(make_pair(*index.first,fields[index.second]));

        result.push_front(map);

    }
    return result;
}

vector<string> TopParser::split_logs(const string &str) {

    vector<string> result;
    string delim = "Processes";

    unsigned long old_pos = 0, pos;
    while((pos = str.find(delim, old_pos + 1)) != string::npos)
    {
        string log = str.substr(old_pos, pos - old_pos);
        result.push_back(log);
        old_pos = pos;
    }
    result.push_back(str.substr(old_pos, string::npos));
    return result;
}

vector<TopData> TopParser::parse_samples(int sample_count) {

    string cmd = "top -l" + to_string(sample_count);
    FILE* top_file = popen(cmd.c_str(), "r");

    auto log_string = string_from_file(top_file);
    auto logs = split_logs(log_string);
    vector<TopData> result;

    for (auto log: logs)
        result.push_back(parse_data(log));

    return result;
}

TopData TopParser::parse() {
    string cmd = "top -l1";
    FILE* top_file = popen(cmd.c_str(), "r");

    auto log_string = string_from_file(top_file);
    return parse_data(log_string);
}

tm TopParser::parse_time(const string& time_str) {

    string format = "%h:%m:%s";
    tm time;

    istringstream is(time_str);
    is >> std::get_time(&time, format.c_str());

    return time;
}












