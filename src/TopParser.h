//
// Created by Prasanth Ravi on 01/08/16.
//

#ifndef POSIXMONITOR_TOPPARSER_H
#define POSIXMONITOR_TOPPARSER_H


#include <cstdio>
#include <string>
#include <forward_list>
#include <unordered_map>

using namespace std;

#include "TopStructures.h"

class TopParser {


public:
    // parse just one log
    virtual TopData parse();
    // parse N samples and get data
    vector<TopData> parse_samples(int);


protected:
    string string_from_file(FILE *file);

    typedef forward_list<string> data_list ;
    typedef forward_list<unordered_map<string,string>> data_map ;

    //parse from string to object data
    virtual TopData parse_data(const string& log) = 0;

    // Parse various section of data
    // These are destructive on the data
    virtual ProcessInfo     parse_proc_info     (data_list &data);
    virtual tm              parse_time_date     (data_list &data);
    virtual LoadAvg         parse_load_avg      (data_list &data);
    virtual CpuUsage        parse_cpu_usage     (data_list &data);
    virtual SharedLibs      parse_shared_libs   (data_list &data);
    virtual MemRegions      parse_mem_regions   (data_list &data);
    virtual PhysicalMem     parse_phy_mem       (data_list &data);
    virtual VirtualMem      parse_vir_mem       (data_list &data);
    virtual Networks        parse_networks      (data_list &data);
    virtual ProcessState    parse_proc_state    (string str);
    virtual Disks           parse_disks         (data_list &data);
    tm                      parse_time          (const string& time_str);


    // parse process headers for each listed process
    data_list   parse_proc_headers(data_list &data);
    // make a map for <header,value> on each process
    data_map    proc_data_to_map(data_list &data,
                                        const data_list &headers);
    // convert map to vector of ProcessData
    virtual
    vector<ProcessData> parse_proc_data(data_map &data_map) = 0;

    // split multi log into a collection of logs
    vector<string> split_logs(const string& str);


};


#endif //POSIXMONITOR_TOPPARSER_H
