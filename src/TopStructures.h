//
// Created by Prasanth Ravi on 01/08/16.
//

#ifndef POSIXMONITOR_TOPSTRUCTURES_H
#define POSIXMONITOR_TOPSTRUCTURES_H

#include <string>
#include <vector>
#include "MemTypes.h"

struct ProcessInfo
{
    int total;
    int running;
    int stuck;
    int sleeping;
    int thread_count;

};

struct LoadAvg
{
    double one_min;
    double five_min;
    double fifteen_min;
};

struct CpuUsage
{
    double user;
    double sys;
    double idle;
};

struct SharedLibs
{
    MemValue resident;
    MemValue data;
    MemValue linkedit;
};

struct MemRegions
{
    int total;
    MemValue resident;
    MemValue pvt;
    MemValue shared;
};

struct PhysicalMem
{
    MemValue used;
    MemValue wired;
    MemValue unused;
};

struct VirtualMem
{
    MemValue vsize;
    MemValue framework_vsize;
    int swapins;
    int swapouts;
};

struct Networks
{
    long in_packets_count;
    MemValue in_total_size;
    long out_packets_count;
    MemValue out_total_size;
};

struct Disks
{
    long read_count;
    MemValue read_size;
    long write_count;
    MemValue write_size;
};

enum class ProcessState
{
    running,
    sleeping,
    stuck,
    dead
};

struct Boosts
{
    int boost_count;
    int transition_count;
    bool sent_boost;
};

// corresponds to one line in the process list
struct ProcessData
{
    int process_id;
    std::string command;
    double cpu_percent;
    tm time;
    int thread_count;
    int workqueue_count;
    int ports;
    MemValue mem;
    MemValue mem_purge;
    MemValue mem_compressed;
    int proc_group;
    int parent_process_id;
    ProcessState state;

    double cpu_me;
    double cpu_others;
    int user_id;
    int faults;
    int cow;
    int msg_sent;
    int msg_recv;
    int page_ins;
    int idlew;
    double power;
    std::string user;

    int kernel_private;
    int kernel_shared;
    int memory_regions;
    int resident_private;
    int virtual_size;
    int virtual_private;
};

// OSX specific attributes for processes
struct MacProcessData : public ProcessData
{
    int csw;
    Boosts boosts;
    int sys_bsd;
    int sys_mach;
};

// parsed object of each Top log
struct TopData
{
    ProcessInfo process_info;
    std::tm time;
    LoadAvg load_average;
    CpuUsage cpu_usage;
    SharedLibs shared_libs;
    MemRegions mem_regions;
    PhysicalMem physical_mem;
    VirtualMem virtual_mem;
    Networks networks;
    Disks disks;
    std::vector<ProcessData> process_list;
};

// structure for additional OSX specific values
struct MacTopData : public TopData
{

};


#endif //POSIXMONITOR_TOPSTRUCTURES_H
