//
// Created by Prasanth Ravi on 01/08/16.
//

#ifndef POSIXMONITOR_UTILS_H
#define POSIXMONITOR_UTILS_H

#include <cstdio>
#include <string>
#include <vector>

using namespace std;

// split a string at a given deliminter
inline vector<string> split_string(string str, const char* tok) {
    vector<string> tokens;

    char* strDup = strdup(str.c_str());
    size_t strSize = sizeof strDup;
    char* nextToken = NULL;
    char* begin = strtok(strDup, tok);
    while (begin) {
        tokens.push_back(string(begin));
        begin = strtok(NULL, tok);
    }
    return tokens;
}

// trim string on both ends
inline string trim_string(const std::string& str)
{
    string::const_iterator start_nw = str.begin();
    while(start_nw != str.end() && isspace(*start_nw))
        start_nw++;

    string::const_reverse_iterator end_nw = str.rbegin();
    while((end_nw.base() != start_nw) && isspace(*end_nw))
        end_nw++;

    return std::string(start_nw, end_nw.base());
}

// get size of std::forward_list
template <class T, class K>
inline int list_size(const std::forward_list<T,K> list)
{
    int count = 0;
    for(auto item: list) count++;
    return count;
}

#endif //POSIXMONITOR_UTILS_H
